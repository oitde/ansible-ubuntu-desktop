#!/bin/bash

  INVENTORY=~/.rapid_ansible_hosts 
  echo localhost ansible_connection=local > $INVENTORY
  if ansible-playbook -i $INVENTORY ./install-xfce.yaml && \
     ansible-playbook -i $INVENTORY ./install-fastx.yaml 
  then
    echo "sucess  Xfce desktop install succeeded" >> /tmp/rapid_image_complete
  else
    echo "fail  playbook failed" >> /tmp/rapid_image_complete
  fi

