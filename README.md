# Ansible Ubuntu Desktop

For use with VCM/RAPID - setup an Ubuntu Xfce desktop for remote access

## The "API" of an installer

These are cloned by the Rapid Installation process and the file named `install.sh` is executed. The file `/tmp/rapid_image_complete` is checked periodically. When this file exists the installer is considered completed.

